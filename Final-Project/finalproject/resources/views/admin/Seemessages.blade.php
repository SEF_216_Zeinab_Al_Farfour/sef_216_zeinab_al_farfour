@extends('layouts.header')

@section('content')


<section id="admin">
<div class="form">
<div class="container">
    <div class="row"> 
        <div class="col-md-12">
       
        <div class="table-responsive">                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
                   
                   <th><input type="checkbox" id="checkall" /></th>
                    <th>user id</th>
                     <th>Subject</th>
                     <th>Body</th>

                   </thead>

    <tr>
    @foreach($contacts as $contact)
    <td><input type="checkbox" class="checkthis" /></td> 
    <td>{{$contact-> user_id}}</td>
    <td>{{$contact-> subject}}</td>
    <td>{{$contact -> body}}</td>
     
    
    </tr>
      @endforeach
    
 
        
</table>

<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</section>

@endsection