<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
   
            $table->increments('id');
            $table->string('text');
            $table->integer('userid')-> unsigned() ;
            $table->foreign('userid')->references('user_id')->on('posts')->onDelete('cascade');
            $table->integer('postidC')-> unsigned() ;
            $table->foreign('postidC')->references('post_id')->on('posts')->onDelete('cascade');
            $table->timestamps();
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('comments');
         $table->dropColumn('userid'); 
         $table->dropColumn('postid'); 
    }
}
