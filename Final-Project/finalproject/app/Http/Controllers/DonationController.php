<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\donation;
use \Illuminate\Http\Auth;
use App\category;
class DonationController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $categories=category::all();
         return view('donation',['categories' =>$categories]);
          return view('donation');

    }

    public function create(Request $request)
        {
            $request->validate(['image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', 'description'=>'required']);
            $donation = new Donation();
            $donation->user_id=auth()->user()->id;
            $donation->category_id=$request->category_id;
            $donation->charity_id=$request->charity_id;
            $donation ->imageURL=$request->file('image')->store('images');
            $donation ->description=$request->description;
            $donation ->save();
           return redirect()->back();;
        }


/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        //
    }
}

