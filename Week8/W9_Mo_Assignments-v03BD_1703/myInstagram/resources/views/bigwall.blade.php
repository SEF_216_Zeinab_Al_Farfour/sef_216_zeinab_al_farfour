@extends('layouts.app')

@section('content')

<section class="row">
    <div class="col-md-12 col-md-offset-3" align="center">
        <h3>All Posts</h3>
        @foreach($posts as $post)
        
        <div class="post">
        <div class="post__name">
        {{$post->user->name}}
        </div>
        <img src="../storage/app/{{$post -> image}}" style="height:300px; width: 300px;">
        <div class="post__description">{{$post -> description}}</div>
        <form action="{{ route('like') }}" method="post" >
        @csrf
        <div align="left">
        <input type="hidden" value="{{ $post-> post_id}}" name="post_id">
        <button type="submit" class="btn btn-primary">like </button>
        </div>
        </form>
        </div>
        <div class="h-seperator"></div>

        @endforeach
    </div>   
</section>
@endsection