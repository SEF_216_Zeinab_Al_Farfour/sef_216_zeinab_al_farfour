@extends('layouts.app')

@section('content')

<section class="row new-post">
    <div class="col-md-12 col-md-offset-3" align="center">
        <header><h3>Enter your post </h3></header>

        <form action="{{ route('post.create') }}" method="post"  enctype="multipart/form-data" >
        @csrf
            <div class="form-group">
                <textarea class="form-contorl" name="description" id="new-post">    
                </textarea>
                <br/>
                <label for="image" class="control-label">choose image : </label>
                <br/>
                <input type="file" id="image" name="image" class="control-label">
                </div>
                <br/><br/>
                <button type="submit" class="btn btn-primary">Create post </button>
                <input type="hidden" value="{{ Session:: token() }}" name="_token">
        </form>
    
    </div>
</section>
@endsection