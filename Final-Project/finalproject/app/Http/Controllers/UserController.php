<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\donation;
use App\recycle;
use Auth;

class UserController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view('/account',['users' =>$users ]);
    }
     
        public function showEditForm()
    {
        $user = User::where('id', Auth::id())->first();
        return view('edit', compact('user'));
    }


        public function showProfileForm()
    {
        $user = User::where('id', Auth::id())->first();
        return view('account', compact('user'));
    }

     public function update(Request $request, $id)
    {
        
        try{
            $user = User::where('id', $id)->first();
            $user->name=$request->input('name');
            $user->phoneNumber=$request->input('phoneNumber');
            $user->DOB=$request->input('DOB');           
            $user->email=$request->input('email');
            $user->password=$request->input('password');
            $user->save();
            $user = User::where('id', Auth::id())->first();
            return redirect()->route('account', [$user]);
        }
        catch(ModelNotFoundException $err){
            
        }
    }

   }
