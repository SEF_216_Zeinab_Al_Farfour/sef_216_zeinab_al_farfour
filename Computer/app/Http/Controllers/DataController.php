<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\data;
use \Illuminate\Http\Auth;
class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('addComputers');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $data = new Data();
        $data->user_id=auth()->user()->id;
        $data->name=$request->name;
        $data->imageURL=$request->file('image')->store('images');
        $data->memory=$request->memory;
        $data->brand=$request->brand;
        $data->modelNumber=$request->modelNumber;
        $data->cpuSpace=$request->cpuSpace;
        $data->save();
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = data::all();
        return view('showComputers',['data' =>$data]);
        return view('showComputers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = data::find($id);
        if($data->delete()){
          return redirect()->back();
        }
    }
}
