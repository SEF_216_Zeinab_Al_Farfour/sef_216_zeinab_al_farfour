<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'PostController@index')->name('home');

Route::post('/createpost', [
    'uses' => 'PostController@create',
    'as' => 'post.create',
    'middleware' => 'auth'
]);

Route::get('/wall', [
    'uses' => 'WallController@index',
    'as' => 'wall',
    'middleware' => 'auth'
]);


Route::get('/bigwall', [
    'uses' => 'BigwallController@index',
    'as' => 'bigwall',
    'middleware' => 'auth'
]);

Route::post('/like', [
    'uses' => 'LikeController@store',
    'as' => 'like',
    'middleware' => 'auth'
]);

Route::get('/', function () {
    return view('welcome');
});
