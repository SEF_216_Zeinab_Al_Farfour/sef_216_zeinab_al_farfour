@extends('layouts.header')

@section('content')
<section id ="trade">
    <div class="intro"> 
    <img src="../public/img/exchange.jpg"/>
    </div>
</section>
<section id ="post">
    <div class="leftrow">
    <h1 class="center">Trade Posts</h1>
      
       @foreach($trades as $trade)
     
        <div class="blog">
          <img src="../storage/app/{{$trade -> imageURL}}"/>
          <div class="v-separator"></div>
          <h1>{{$trade -> title}}</h1>
          <p> {{$trade -> age}} </p>
          <p> {{$trade -> description}}</p>
          <p> {{$trade -> type}}</p>
          <p> {{$trade -> phoneNumber }}</p>
         <div class="h-separator"></div>
        </div>         
        @endforeach
    </div>
    
  

</section>
       
@endsection