<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', function () {
    return view('home');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::get('showComputers', ['uses' => 'DataController@show','as' => 'show']);
Route::post('addComp', ['uses' => 'DataController@create','as' => 'comp.add']);

Route::get('addComputers', ['uses' => 'DataController@index','as' => 'add']);
Route::get('/delete-post/{id}', [
    'uses' => 'DataController@destroy',
    'as' => 'post.delete',
    'middleware' => 'auth'
]);