<?php

namespace App\Http\Controllers;
use App\post;
use Illuminate\Http\Request;

class WallController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $posts = post::all();
        return view('wall',['posts' =>$posts]);
    
    }
}