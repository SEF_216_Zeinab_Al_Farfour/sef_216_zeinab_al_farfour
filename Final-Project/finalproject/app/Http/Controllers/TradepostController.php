<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\trade;
use App\category;
use App\User;
use App\Auth;



class TradepostController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trades = trade::join('categories', 'trades.category_id', '=', 'categories.id')
        ->join('users','trades.user_id', '=', 'users.id')
        ->select('users.id','trades.id','imageURL','title', 'age', 'description', 'type','phoneNumber')->get();
        return view('tradeposts',['trades' => $trades]);
    
    }
}