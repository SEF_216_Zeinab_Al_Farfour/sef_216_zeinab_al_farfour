#!/usr/bin/php
<?php
require_once "DataBase.php";
require_once "Table.php";

$myDataBase = new DataBase();
$myTable = new Table();
$myDataBaseName = " ";
$myTableName = " ";

$input = readline("Enter statment or -1 to exit: ");

while ($input != -1)
{
	$line = explode(",", $input);

	//Choosing database 
	if ($line[0] == "USE" && $line[1] == "DATABASE")
	{
		$dabaseName = trim($line[2], "\"");
		if ($myDataBase->DbExists($dabaseName))
		{
			$myDataBaseName = $dabaseName;
			$input = readline("Enter statment or -1 to exit: ");
		}
		else
		{
			echo "Database doesn't exist";
			$input = readline("Enter statment or -1 to exit: ");
	}
}
	//Choosing table
	else if ($line[0] == "USE" && $line[1] == "TABLE")
	{
		if ($myDataBaseName != -1)
		{
			$tableName = trim($line[2], "\"");
			if ($myTable->TbExists($myDataBaseName, $tableName))
			{
				$myTableName = $tableName;
				$input = readline("Enter statment or -1 to exit: ");
			}
			else
			{
				echo "Table doesn't exist!\n";
				$input = readline("Enter statment or -1 to exit: ");
			}
		}
		else
		{
			echo " Select Database ";
			$input = readline("Enter statment or -1 to exit: ");
		}
	}
	//Creating Database
	else if ($line[0] == "CREATE" && $line[1] == "DATABASE")
	{
		$databaseName = trim($line[2], "\"");
		$myDataBase->createDatabase($databaseName);
		$input = readline("Enter statment or -1 to exit: ");
	}

	//Creating Table in Database
	else if ($line[0] == "CREATE" && $line[1] == "TABLE")
	{
		if ($myDataBaseName != -1)
		{
			$tableName = trim($line[2], "\"");
			$columns = array();
			$j = 0;
			for ($i = 2; $i < count($line); $i++)
			{
				$columns[$j] = trim($line[$i], "\"");
				$j++;
			}

			$myTable->createTable($myDataBaseName, $tableName, $columns);

			$input = readline("Enter statment or -1 to exit: ");
		}
		else
		{
			echo "Select Database \n";
		$input = readline("Enter statment or -1 to exit: ");
		}
	}

	//Deleting DataBase
	else if ($line[0] == "DELETE" && $line[1] == "DATABASE")
	{
		$databaseName = trim($line[2], "\"");
		if ($myDataBase->DbExists($databaseName))
		{
			$myDataBase->deleteDatabase($databaseName);
		    $input = readline("Enter statment or -1 to exit: ");
		}
		else
		{
			echo "Database exist\n";
			$input = readline("Enter statment or -1 to exit: ");
		}
	}

//Adding Rows 
	else if ($line[0] == "ADD" && $line[1] == "RECORD")
	{
		if ($myTableName != -1)
		{
			if ($myTable->TbExists($myDataBaseName, $myTableName))
			{
				$data = array();
				$j = 0;
				for ($i = 2; $i < count($line); $i++)
				{
					$data[$j] = trim($line[$i], "\"");
					$j++;
				}
				$myTable->addRow($myDataBaseName, $myTableName, $data);

				$input = readline("Enter statment or -1 to exit: ");
			}
			else
			{
				echo "Table doesn't exists\n";
				$input = readline("Enter statment or -1 to exit: ");
			}
		}
		else
		{
			echo "Select Table \n";
			$input = readline("Enter statment or -1 to exit: ");
		}
	}



}