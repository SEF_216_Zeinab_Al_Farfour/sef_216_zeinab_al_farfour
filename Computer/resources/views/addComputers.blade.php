@extends('layouts.app')
@section('content')
        <section id="form">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <img src="../public/img/images.jpeg" />
                </div>
            <div class="col-md-4 col-sm-6">  
                <div class="fill">
                  <form action="{{ route('comp.add') }}" method="post"  enctype="multipart/form-data">
                  @csrf
    
                    <label for="name">Name :</label>
                    <input type="text"  required="required" name="name" placeholder="name">
                    <br/>
                    <label for="brand">Brand:</label>
                    <input type="text"  required="required" name="brand" placeholder="brand">
                    <br/>
                    <label for="modelNumber">Model Number :</label>
                    <input type="text"  required="required" name="modelNumber" placeholder="modelNumber">
                    <br/>
                    <label for="memory">Memory:</label>
                    <input type="text"  required="required" name="memory" placeholder="memory">
                    <br/>
                    <label for="cpuSpace">CPU Space:</label>
                    <input type="text"  required="required" name="cpuSpace" placeholder="cpuSpace">
                    <br/>
                    <label for="image">Choose image : </label>
                    <input type="file"  required="required" name="image" placeholder="image">
                    </br>  
                    <input type="submit" value="Submit">
                    <input type="hidden" value="{{ Session:: token() }}" name="_token" >
                  </form>
                
              </div>
             </div>
            </div>
       
    </section>
@endsection