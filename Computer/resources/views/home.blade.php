@extends('layouts.app')

@section('content')
    @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
    @endif
    <section id="show">
        <div class="intro">
            <h2>Welcome to our store</h2>
        </div>
    </section>
@endsection
