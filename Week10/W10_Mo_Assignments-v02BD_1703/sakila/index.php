<?php

class index{
    public function getBasePath()
    {
        $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
        return $basepath;
    }

    public function getCurrentUri($basepath)
    {
        $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
        if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
        $uri = '/' . trim($uri, '/');
        return $uri;
    }
 public function getUriRoutes($base_url)
    {
        $routes = array();
        $routes = explode('/', $base_url);
        return $routes;
    }
}
?>