<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
require_once'../config/database.php';
require_once '../customer.php';

$database = new Database();
$db = $database->getConnection();

$customer = new Customer($db);
$data = json_decode(file_get_contents("./test.txt"));

$customer ->store_id = $data->store_id;
$customer ->first_name = $data->first_name;
$customer ->last_name = $data->last_name;
$customer ->email = $data->email;
$customer ->address_id = $data->address_id;

if($customer ->create()){
        echo "customer was created ";
}
 
else{
	echo "Unable to create customer";
}
?>
