@extends('layouts.header')

@section('content')


<section id="admin">
<div class="form">
<div class="container">
    <div class="row"> 
        <div class="col-md-12">
       
        <div class="table-responsive">                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
                   
                   <th><input type="checkbox" id="checkall" /></th>
                  <th>user id</th>                      
                   <th>Image</th>
                    <th>Description</th>
                     <th>Charity</th>
                      <th>Category</th>
                   </thead>

    
    <tr>
    @foreach($donations as $donate)
    <td><input type="checkbox" class="checkthis" /></td> 
    <td>{{$donate -> user_id}}</td>
   
    <td><img src="../storage/app/{{$donate -> imageURL}}" style="height:30px; width: 30;"></td>
    <td>{{$donate -> description}}</td>
    <td>{{$donate -> charity_id}}</td>
     <td>{{$donate -> category_id}}</td>
    
 </tr>
     @endforeach
        
</table>

<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</section>

@endsection