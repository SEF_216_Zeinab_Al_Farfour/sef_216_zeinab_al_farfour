@extends('layouts.header')

@section('content')


<section id="admin">
<div class="form">
<div class="container">
    <div class="row"> 
        <div class="col-md-12">
       
        <div class="table-responsive">                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
                   
                   <th><input type="checkbox" id="checkall" /></th>
                   <th>user id</th>
                   <th>Time</th>
                    <th>Date</th>
                     <th>Address</th>
                     <th>Description</th>
  
                   </thead>

    
    <tr>
    @foreach($recycles as $recycle)
    <td><input type="checkbox" class="checkthis" /></td> 
    <td>{{$recycle -> user_id}}</td>
    <td>{{$recycle -> time}}</td>
    <td>{{$recycle -> date}}</td>
    <td>{{$recycle -> address}}</td>
    <td>{{$recycle -> description}}</td>
       </tr> 
           @endforeach
</table>

<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</section>

@endsection