@extends('layouts.header')

@section('content')
<section id ="admin">
<div class="form">
   <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    
                </div>              
                 <!-- /. ROW  -->
                  <hr />
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="alert alert-info">
                             <strong>Welcome Admin ! </strong>
                        </div>
                       
                    </div>
                    </div>
                  <!-- /. ROW  --> 
                  <div class="row text-center pad-top">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="div-square">
                           <a href="{{ route('seeusers') }}" >
 						               <i class="fa fa-users fa-5x"></i>
                      <h4>See Users</h4>
                      </a>
                      <input type="hidden" value="{{ Session:: token() }}" name="_token"> 
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="div-square">
                           <a href="{{ route('seemessages') }}" >
		               				 <i class="fa fa-envelope-o fa-5x"></i>
                      <h4>See messages </h4>
                      </a>
                       <input type="hidden" value="{{ Session:: token() }}" name="_token"> 
                   </div>
                  </div>
                 
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                     
                      <div class="div-square">
                       <a href="{{ route('getcat') }}" >
                     @csrf
							       <i class="fa fa-plus fa-5x"></i>
                      <h4>Add categories</h4>
                      </a>

                     <input type="hidden" value="{{ Session:: token() }}" name="_token">
                   </div>
                  </div>
                  
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="div-square">
                           <a href="{{ route('getchar') }}" >
                       @csrf
							             <i class="fa fa-plus fa-5x"></i>
                      <h4>Add charities</h4>
                      </a>
                      <input type="hidden" value="{{ Session:: token() }}" name="_token">
                      </div>  
                </div>
                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="div-square">
                           <a href="{{ route('seedonations') }}">
						 <i class="fa fa-circle-o-notch fa-5x"></i>
                      <h4>Check Donation</h4>
                      </a>
                       <input type="hidden" value="{{ Session:: token() }}" name="_token">
                      </div>
                     
                     
                  </div> 
                 

                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                      <div class="div-square">
                    <a href="{{ route('seerecycles') }}" >
                     @csrf
 						        <i class="fa fa-circle-o-notch fa-5x"></i>
                      <h4>Check Recycle</h4>
                      </a>
                    <input type="hidden" value="{{ Session:: token() }}" name="_token">
                      </div>
                     
                     
                  </div>

                  <!-- /. ROW  --> 
               </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
        </div>
</section>
    
@endsection
