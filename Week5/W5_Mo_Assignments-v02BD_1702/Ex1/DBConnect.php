<?php

class DB_Connect
{
	function __construct()
	{

	}

	public function connect()
	{
		require_once 'config.php';
		$db_connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
		return $db_connection;
	}

	public function close()
	{
		mysql_close();
	}
}	