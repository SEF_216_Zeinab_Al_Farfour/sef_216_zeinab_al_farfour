<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\category;
use App\charity;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function admin()
    { 
        return view('admin');
    }

    public function trade()

    {   $categories=category::all();
        return view('trade',['categories' =>$categories]);
        return view('trade');
    }
    public function recycle()

    { 
        return view('recycle');
    }
     public function donation()

    {   $categories=category::all();
        $charities=charity::all();
        return view('donation',['categories' =>$categories,'charities' =>$charities]);
        return view('donation');
    }



}
