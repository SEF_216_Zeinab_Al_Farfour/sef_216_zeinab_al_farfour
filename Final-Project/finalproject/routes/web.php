<?php
use App\user;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@admin')->middleware('admin');
Route::get('/index', ['uses' => 'IndexController@index', 'as' => 'index']);
Route::get('/aboutus', ['uses' => 'AboutusController@index', 'as' => 'aboutus']);
Route::get('/trade1', ['uses' => 'TradeController@index','as' => 'trade1']);
Route::post('/trade', ['uses' =>'TradeController@create','as' => 'trade.create']);
Route::post('/recycle1', ['uses' =>'RecycleController@create','as' => 'recycle.create']);
Route::get('/recycle', ['uses' =>'RecycleController@index']);

Route::post('/donation1', ['uses' =>'DonationController@create','as' => 'donation.create']);
Route::get('/donation', ['uses' =>'DonationController@index']);

Route::get('/tradeposts', ['uses' =>'TradepostController@index','middleware' => 'auth','as' => 'tradeposts']);


Route::get('/clickdonation', ['uses' =>'HomeController@donation','as' => 'clickdonate','middleware' => 'auth']);
Route::get('/clicktrade', ['uses' =>'HomeController@trade','as' => 'clicktrade','middleware' => 'auth']);
Route::get('/clickrecycle', ['uses' =>'HomeController@recycle','as' => 'clickrecycle','middleware' => 'auth']);

Route::get('/mytrades', ['uses' =>'MytradesController@index','as' => 'mytrades','middleware' => 'auth']);

Route::get('/delete-post/{id}', [
    'uses' => 'MytradesController@destroy',
    'as' => 'post.delete',
    'middleware' => 'auth'
]);

Route::get('/admin3', ['uses' =>'AdminController@donations','as'=>'seedonations','middleware' => 'admin']);
Route::get('/admin1', ['uses' =>'AdminController@recycles','as'=>'seerecycles','middleware' => 'admin']);
Route::get('/admin2', ['uses' =>'AdminController@users','as'=>'seeusers','middleware' => 'admin']);
Route::get('/admin4', ['uses' =>'AdminController@messages','as'=>'seemessages','middleware' => 'admin']);

Route::get('/categories', ['uses' => 'AdminController@getcat','as' => 'getcat','middleware' => 'admin']);
Route::post('/addcategories', ['uses' =>'AdminController@AddCategories','as' => 'cat.create','middleware' => 'admin']);

Route::get('/charities', ['uses' => 'AdminController@getcharities','as' => 'getchar','middleware' => 'admin']);
Route::post('/addcharities', ['uses' =>'AdminController@AddCharities','as' => 'char.create','middleware' => 'admin']);


Route::get('/edit', 'UserController@showEditForm')->name('edit');
Route::get('/account', 'UserController@showProfileForm')->name('account');

Route::post('/edit/{id}', 'UserController@update')->name('profile.edit');

Route::get('/contactus', ['uses' => 'ContactusController@index','as' => 'contactus']);
Route::post('/contact', ['uses' =>'ContactusController@create','as' => 'contact.create']);