<?php

class GameGenerator
{
  protected $list1;
  protected $list2;

  function __construct()
  {
    $this->list1 = array(25, 50, 75, 100);
    $this->list2 = array(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10);
  }

  function generateList()
  {
    $random = rand(1, 4);
    $randomElse = 6 - $random;
    $newList1 = array_rand($this->list1, $random);
    $newList2 = array_rand($this->list2, $randomElse);

    for($i = 0; $i < $random; $i++)
    {
      if($random == 1)
        $newList[$i] = $this->list1[$newList1];
      else 
        $newList[$i] = $this->list1[$newList1[$i]]; 
    }
    $list2index = 0;
    for($j = $random; $j < 6; $j++)
    {
      $newList[$j] = $this->list2[$newList2[$list2index]];  
      $list2index++;
    }

    return $newList;
  }

  function generateNum()
  {
    $number = rand(101, 999);
    return $number;
  }
}

