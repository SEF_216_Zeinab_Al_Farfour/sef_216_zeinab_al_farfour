@extends('layouts.header')

@section('content')
 <section id="form">

        <div class="row">
            
            <div class="col-md-4 col-sm-6">
                <img src="../public/img/b.jpg" />
            </div>
            <div class="col-md-4 col-sm-6">  
                <div class="fill">
                  <form action="{{ route('recycle.create') }}" method="post"  enctype="multipart/form-data">
                  @csrf
                    
                    <label for="description">Description :</label>
                    <input type="text"  required="required" name="description" placeholder="Description">
                    
                     <label for="address">Address :</label>
                    <input type="text"  required="required" name="address" placeholder="Address">
                    
                     <label for="date">Date :</label>
                    <input type="date"  required="required" name="date" placeholder="Date">
                    </br>
                    <label for="time">Time :</label>
                    <input type="text"  required="required" name="time" placeholder="time">

                    </br>  
                    <input type="submit" value="Submit">
                    <input type="hidden" value="{{ Session:: token() }}" name="_token">
                  </form>
                </div>
              </div>
            </div>
       
    </section>
    @endsection