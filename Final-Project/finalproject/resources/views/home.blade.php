@extends('layouts.header')
@section('content')

<section id ="home">
    <div class="intro"> 
    <img src="../public/img/a.jpg"/>
    </div>
</section>

<section class="home">
<div class="gridcontainer clearfix">
            <div class="titleh"><h1>CHOOSE HOW YOU WANT TO MAKE THE BEST OF YOUR ITEMS</h1></div>
            <div class="h_seperatorh" ></div>

    <div class="grid_3">
        <div class="fmcircle_out">
             <form action="{{ route('clickrecycle') }}"   enctype="multipart/form-data" >
                @csrf
                <div class="fmcircle_border">
                    <div class="fmcircle_in fmcircle_blue">
                        <button class="home-button">Recycle </button>
                      <input type="hidden" value="{{ Session:: token() }}" name="_token">   
                    </div>
                </div>
        
           </form>
        </div>
    </div>

    <div class="grid_3">
        <form action="{{ route('clicktrade') }}"   enctype="multipart/form-data" >
                @csrf
        <div class="fmcircle_out">
           
                <div class="fmcircle_border">
                    <div class="fmcircle_in fmcircle_green">
                    <button class="home-button">Trade </button>
                    <input type="hidden" value="{{ Session:: token() }}" name="_token">   
                    </div>
                </div>
           
             </form>
        </div>
    </div>

    <div class="grid_3">
        <div class="fmcircle_out">
            <form action="{{ route('clickdonate') }}" enctype="multipart/form-data" >
                @csrf
         
                <div class="fmcircle_border">
                    <div class="fmcircle_in fmcircle_red">
                        <button class="home-button">Donation </button>
                        <input type="hidden" value="{{ Session:: token() }}" name="_token">   
                    </div>
                </div>
    
            </form>
        </div>
    </div>
</div>
  </section>
<section id="footer">
                <div class="grid960">
                    <div class="container__footer__column-left">
                    
 
                        <p>Lorem ipsum dolor sit amet, consecteturadipisicing elit. Excepturi quasi delectus iusto incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       
                    </div>
                    <div class="container__footer__column-middle">
                        <h3>OUR STUDIO</h3>
                        <table class="container__footer__column-middle__location">
                            <tr>

                                <td>
                                    <p>Lorem ipsum dolor sit amet, consecteturadipisicing elit. Excepturi quasi delectus</p>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <p>(+62) 21-2224 3333</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                
                    <div class="container__footer__column-right">
                        <h3>STAY IN TOUCH</h3>
                        <input type="text" value="Subscribe our newsletter">
                        
                        <div class="container__footer__column-right__media ">
                           
                        </div>
                          
                    </div>
                
                </div>
            </section><!--/END container__footer -->


@endsection