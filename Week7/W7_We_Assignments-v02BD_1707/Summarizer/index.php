<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Summerizer</title>
  <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
<section class="container">
	<div class="container__url">
		<h1>Summerizer</h1>
		<p>please enter the url to summerize</p>
		<div align="center">
			<form>
				<input type='text' id="url" value= "url">
				<button onclick="submit()">submit </button>
			</form>
		 
		</div>
		<div id="summary"> </div>
	</div>

</section>
<script type="js/script.js"></script>
</body>
</html>
