@extends('layouts.header')
     @section('content')
    <section id="form">

        <div class="row">
            
            <div class="col-md-4 col-sm-6">
                <img src="../public/img/e.jpg" />
            </div>
            <div class="col-md-4 col-sm-6">  
                <div class="fill">
                  <form action="{{ route('donation.create') }}" method="post"  enctype="multipart/form-data">
                  @csrf
                    
                    <label for="description">Description :</label>
                    <input type="text"  required="required" name="description" placeholder="Description">
                    
                    <label for="country">Category :</label>
                    <select required="required" name="category_id">
                    @foreach($categories as $category)
                    <option value= "{{$category -> id}}">{{$category -> type}}</option>
                     @endforeach
                    </select>

                    <label for="country">Charity :</label>
                    <select required="required" name="charity_id">
                    @foreach($charities as $charity)
                    <option value= "{{$charity-> id}}">{{$charity -> name}}</option>
                     @endforeach
                    </select>

                    <label for="description">Choose image : </label>
                    <input type="file"  required="required" name="image" placeholder="Description">

                    </br>  
                    <input type="submit" value="Submit">
                    <input type="hidden" value="{{ Session:: token() }}" name="_token">
                  </form>
                </div>
              </div>
            </div>
       
    </section>
@endsection