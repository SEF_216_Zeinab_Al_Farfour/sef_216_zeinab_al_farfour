@extends('layouts.header')
     @section('content')
    <section id="form">

        <div class="row">
            
            <div class="col-md-4 col-sm-6">
                <img src="../public/img/g.jpg" />
            </div>
            <div class="col-md-4 col-sm-6">  
                <div class="fill">
                  <form action="{{ route('trade.create') }}" method="post"  enctype="multipart/form-data">
                  @csrf
                    <label for="title">Title : </label>
                    <input type="text"  required="required" name="title"" placeholder="Item title">

                    <label for="age">Age :</label>
                    <input type="text"  required="required" name="age" placeholder="Item age">
                    
                    <label for="description">Description :</label>
                    <input type="text"  required="required" name="description" placeholder="Description">
                    
                    <label for="country">Category :</label>
                    <select required="required" name="category_id">
                    @foreach($categories as $category)
                    <option value= "{{$category -> id}}">{{$category -> type}}</option>
                     @endforeach
                    </select>
                    <label for="description">Choose image : </label>
                    <input type="file"  required="required" name="image" placeholder="Description">

                    </br>  
                    <input type="submit" value="Submit">
                    <input type="hidden" value="{{ Session:: token() }}" name="_token" >
                  </form>
                </div>
              </div>
            </div>
       
    </section>
@endsection