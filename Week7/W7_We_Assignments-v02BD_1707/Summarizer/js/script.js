
function submit() {
  var url = document.getElementById('url').value;
  var request = new XMLHttpRequest();
  request.open("POST", "php/gettingHTML.php", true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      if (request.status === 200) {
        document.body.className = 'ok';
        var content = request.responseText;
  
        parse(content);
      }
      else {
         console.log("Error"); 
      }
    }
  }
  request.send(url);
}

function parse(content) {
  var final = "";
  var parser = new DOMParser();
  var doc = parser.parseFromString(content, "text/html");
  var area = doc.getElementsByClassName('postArticle-content')[0];
  var arr = area.getElementsByTagName('p');
  for(var i = 0; i < arr.length; i++){
    final += arr[i].textContent + "\n";
  }

  var title = doc.getElementsByTagName("h1")[0].textContent;

}

