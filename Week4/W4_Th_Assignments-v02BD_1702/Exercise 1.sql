create database Patients;

use Patients;

#Create  Tables 

CREATE TABLE Claims (
    claim_id INT(5),
    patient_name VARCHAR(50),
    PRIMARY KEY (claim_id)
);

CREATE TABLE Defendants (
    claim_id INT(5),
    defendant_name VARCHAR(50),
    PRIMARY KEY (claim_id , defendant_name)
);

CREATE TABLE LegalEvents (
    claim_id INT(5),
    defendant_name VARCHAR(50),
    claim_status VARCHAR(50),
    change_date DATE
);

CREATE TABLE ClaimStatusCodes (
    claim_status VARCHAR(50),
    claim_status_desc VARCHAR(100),
    claim_seq INT(5),
    PRIMARY KEY (claim_status)
);

#insert data into tables

insert into Claims value(1, 'Bassem Dghaidi' );
insert into Claims value(2, 'Omar Breidi'  );
insert into Claims value(3, 'Marwan Sawwan' );

insert into Defendants values (1,'Jean skaff');
insert into Defendants values (1,'Elie Meouchi');
insert into Defendants values (1,'Radwan Sameh');
insert into Defendants values (2,'Joseph Eid');
insert into Defendants values (2,'Paul Syoufi');
insert into Defendants values (2,'Radwan Sameh');
insert into Defendants values (3,'Issam Awwad');

insert into LegalEvents values(1 ,'Jean Skaff', 'AP','2016-01-01');
insert into LegalEvents values(1,'Jean Skaff','OR','2016-02-02' );
insert into LegalEvents values(1,'Jean Skaff','SF','2016-03-01');
insert into LegalEvents values( 1,'Jean Skaff','CL','2016-04-01');
insert into LegalEvents values(1,'Radwan Sameh','AP','2016-01-01');
insert into LegalEvents values(1,'Radwan Sameh','OR','2016-02-02');
insert into LegalEvents values(1 ,'Radwan Sameh','SF','2016-03-01');
insert into LegalEvents values(1,'Elie Meouchi','AP','2016-01-01');
insert into LegalEvents values(1,'Elie Meouchi','OR','2016-02-02');
insert into LegalEvents values(2,'Radwan Sameh','AP','2016-01-01');
insert into LegalEvents values(2,'Radwan Sameh','OR','2016-02-01');
insert into LegalEvents values(2,'Paul Syoufi','AP','2016-01-01');
insert into LegalEvents values(3,'Issam Awwad','AP','2016-01-01');

insert into ClaimStatusCodes value('AP','Awaiting review panel',1);
insert into ClaimStatusCodes value('OR','Panel opinion rendered',2);
insert into ClaimStatusCodes value('SF','Suit filed',3);
insert into ClaimStatusCodes value('CL','Closed',4);

#Query

SELECT 
    TABLE2.claim_id,
    TABLE2.patient_name,
    ClaimStatusCodes.claim_status
FROM
    (SELECT 
           TABLE1.claim_id,
            TABLE1.patient_name,
            MIN(TABLE1.ClaimSeq) AS MINCLAIM
    FROM
        (SELECT 
           Claims.patient_name,
            Claims.claim_id,
            MAX(ClaimStatusCodes.claim_seq) AS ClaimSeq
    FROM
        ClaimStatusCodes
    INNER JOIN LegalEvents ON ClaimStatusCodes.claim_status = LegalEvents.claim_status
    INNER JOIN Claims ON LegalEvents.claim_id = Claims.claim_id
    GROUP BY Claims.claim_id , LegalEvents.defendant_name) AS TABLE1
    GROUP BY TABLE1.claim_id) AS TABLE2
        INNER JOIN
    ClaimStatusCodes ON TABLE2.MINCLAIM = ClaimStatusCodes.claim_seq
ORDER BY TABLE2.claim_id;
                

