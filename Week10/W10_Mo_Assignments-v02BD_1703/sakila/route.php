<?php

require_once './index.php';

$index = new Index();

$host  = $_SERVER['HTTP_HOST'];
$basepath = $index->getBasePath();

$base_url = $index->getCurrentUri($basepath);
$routes = $index->getUriRoutes($base_url);


if ($routes[1] == "customer") {
    $controller = 'customer/';
    
    if ($routes[2] == "read") {
        $extra = 'read.php';
        header("Location: http://$host$basepath$controller$extra");
    }
    elseif ($routes[2] == "create") {
        $extra = 'create.php';
        header("Location: http://$host$basepath$controller$extra");
    }
    elseif ($routes[2] == "update") {
        $extra = 'update.php';
        header("Location: http://$host$basepath$controller$extra");
    }
    elseif ($routes[2] == "delete") {
        $extra = 'delete.php';
        header("Location: http://$host$basepath$controller$extra");
    }
}
?>