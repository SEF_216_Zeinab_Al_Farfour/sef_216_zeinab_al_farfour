<?php
require_once "Gamegenerator.php";
class GameSolver
{
  protected $opList;
  protected $numList;
  protected $number;

  function __construct(GameGenerator $generate)
  {
    $this->opList = array("+", "-", "*", "/");
    $this->numList = $generate->generateList();
    $this->number = $generate->generateNum();
  } 

  function permute($items, $perms = array()) 
  {
    if (empty($items)){
      $permutation= array($perms);
    }  
    else{
      $permutation = array();
      for ($i = count($items) - 1; $i >= 0; --$i){
        $newitems = $items;
        $newperms = $perms;
        list($permute) = array_splice($newitems, $i, 1);
        array_unshift($newperms, $permute);
        $permutation = array_merge($permutation, $this->permute($newitems, $newperms));
      }
    }
    return $permutation;
  }

  function combination($chars, $size, $combo = array()) 
  {
  
    if (empty($combo)){
      $combo = $chars;
    }
    if ($size == 1){
      return $combo;
    }

    $new_combinations = array();

    foreach ($combo as $combo){
      foreach ($chars as $char){
        $new_combinations[] = $combo . $char;
      }
    }
    return $this->combination($chars, $size - 1, $new_combinations);
  }


  function solveList()
  {
    echo "numbers are:". "\n" ;
    print_r($this->numList);
    echo "Target is :" . "\n" . "$this->number" . "\n";

   
  }

}