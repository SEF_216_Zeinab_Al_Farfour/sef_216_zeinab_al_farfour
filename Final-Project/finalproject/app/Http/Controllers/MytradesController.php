<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\trade;
use App\category;
use App\auth;



class MytradesController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $trades = trade::join('categories', 'trades.category_id', '=', 'categories.id')
        ->join('users','trades.user_id', '=', 'users.id')

        ->select('users.id','trades.id','imageURL','title', 'age', 'description', 'type','user_id')->get();

       
     return view('mytrades',['trades' => $trades]);


    
    }
    public function destroy($trade_id)
    {
        $trade = trade::find($trade_id);
        if($trade->delete()){
            redirect()->route('mytrades')->with(['message' => 'Successfully deleted!']);
        }
        // $trade = trade::where('id','=', $id)->first();
        // if (Auth::user() != $trade->user_id) {
        //     return redirect()->back();
        // }
        // $trade->destroy();
        // return redirect()->route('mytrades')->with(['message' => 'Successfully deleted!']);
    }

}