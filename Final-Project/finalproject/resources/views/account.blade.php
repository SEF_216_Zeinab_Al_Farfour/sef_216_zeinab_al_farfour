@extends('layouts.header')

@section('content')
<section id="profile">
<div class="card">
    <div class="box">
        <center><i class="fa fa-user-circle-o" style="font-size:48px"></i></center>

        <center><h2>{{$user -> name}}</h2></center> 
         <label>Email :</label> <p> {{$user -> email}} </p>
         <label>DOB :</label> <p> {{$user ->DOB}}</p>
          <label>Phone Number : </label><p> {{$user -> phoneNumber }}</p>
    </br>

          @csrf
          <a href="{{ route('edit') }}" >
          <h2><br><span>Edit</span></h2>
          </a>
          <input type="hidden" value="{{ Session:: token() }}" name="_token"> 
         
    </div>
</div>
</section>
@endsection
