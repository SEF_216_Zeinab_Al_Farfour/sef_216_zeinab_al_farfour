<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')-> unsigned() ;
            $table->foreign('userid')->references('user_id')->on('posts')->onDelete('cascade');
            $table->integer('postid')-> unsigned() ;
            $table->foreign('postid')->references('post_id')->on('posts')->onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('likes');
          $table->dropColumn('userid'); 
          $table->dropColumn('postid'); 
    }
}
