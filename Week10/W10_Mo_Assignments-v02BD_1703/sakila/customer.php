<?php
class Customer{
 
       
        private $conn;
        private $table_name = "customer";
        public $customer_id;
        public $store_id;
        public $first_name;
        public $last_name;
        public $email;
        public $address_id;
        public $active;
        public $create_date;
        public $last_update;
     

        
        public function __construct($db){
            $this->conn = $db;
     }
        
    function read(){
     
        $query = "SELECT
                        *
                    FROM
                         $this->table_name "; 
          
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;

    }

    function create(){
        
        $query= "INSERT INTO ". $this->table_name . "(store_id, first_name, last_name,email,address_id)  
        VALUES (:store_id,:first_name,:last_name,:email,:address_id)";
     
        $stmt = $this->conn->prepare($query);

        
        $this->store_id=htmlspecialchars(strip_tags($this->store_id));
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->address_id=htmlspecialchars(strip_tags($this->address_id));
     
       
        $stmt->bindParam(":store_id", $this->store_id);
        $stmt->bindParam(":first_name", $this->first_name);
        $stmt->bindParam(":last_name", $this->last_name);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":address_id", $this->address_id);
     
        
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    } 

    function update(){
     
     
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    store_id = :store_id,
                    first_name = :first_name,
                    last_name = :last_name,
                    email = :email,
                    address_id = :address_id
                    
                WHERE
                    customer_id = :customer_id";
     
       
        $stmt = $this->conn->prepare($query);
            

        
        $this->customer_id=htmlspecialchars(strip_tags($this->customer_id));
        $this->store_id=htmlspecialchars(strip_tags($this->store_id));
        $this->first_name=htmlspecialchars(strip_tags($this->first_name));
        $this->last_name=htmlspecialchars(strip_tags($this->last_name));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->address_id=htmlspecialchars(strip_tags($this->address_id));
     
        
        $stmt->bindParam(":customer_id", $this->customer_id);
        $stmt->bindParam(":store_id", $this->store_id);
        $stmt->bindParam(":first_name", $this->first_name);
        $stmt->bindParam(":last_name", $this->last_name);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":address_id", $this->address_id);
     
      
        if($stmt->execute()){
            return true;
            
        }
     
        return false;
    }
    
    function delete(){

        $query = "DELETE FROM " . $this->table_name . " WHERE customer_id = ?";
        $stmt = $this->conn->prepare($query);
        $this->customer_id=htmlspecialchars(strip_tags($this->customer_id));
        $stmt->bindParam(1, $this->customer_id);
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
}

?>