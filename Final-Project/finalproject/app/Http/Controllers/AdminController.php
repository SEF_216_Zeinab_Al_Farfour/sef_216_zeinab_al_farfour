<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use App\donation;
use App\recycle;
use App\category;
use App\charity;
use App\Contact;
use \Illuminate\Http\Auth;

class AdminController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function donations()
    {

    $donations = donation::all();
    return view('admin/seeDonations',['donations' =>$donations]);
    
    }
    public function recycles()
    {
        $recycles = recycle::all();
    return view('admin/seeRecycle',['recycles' =>$recycles]);
     
    }
    public function messages()
    {
        $contacts= Contact::all();
    return view('admin/Seemessages',['contacts' =>$contacts]);
     
    }

    public function users()
    {
        $users = User::all();
    return view('admin/seeUser',['users' =>$users ]);
     
    }

    public function getcat(){

         $categories=category::all();
          return view('addCategories');
    }

        public function getcharities(){

         $charities=charity::all();
          return view('addCharities');
    }

    public function AddCategories(Request $request)
        {
        
            $category = new Category();
            $category->type=$request->type;
            $category->save();
          return redirect()->back();
        }

        public function AddCharities(Request $request)
        {
        
            $charity = new Charity();
            $charity->name=$request->name;
            $charity->description=$request->description;
            $charity->phoneNumber=$request->phoneNumber;
            $charity->save();
            return redirect()->back();
        }
}