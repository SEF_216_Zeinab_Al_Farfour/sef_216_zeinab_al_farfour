@extends('layouts.app')

@section('content')

<section class="row">
    <div class="col-md-12 col-md-offset-3" align="center">
        <h3>My Posts</h3>
        @foreach($posts as $post)
        @if($post->user_id==Auth::user()->id)
        <div class="post">
        <div class="post__name">
        {{$post->user->name}}
        </div>
        <img src="../storage/app/{{$post -> image}}" style="height:300px; width: 300;">
        <div class="post__description">{{$post -> description}}</div>
        <div class="h-seperator"></div>
        @endif
        @endforeach

    </div>   
</section>

@endsection