@extends('layouts.header')

@section('content')
<section id="profile">
<div class="card">  

    <div class="box1">
      <div class="icon">
      <center><i class="fa fa-user-circle-o" style="font-size:48px"></i></center>
      </div>
 
       <form   enctype="multipart/form-data" id="modal_form_id" method="POST" action="{{url('edit/'.Auth::user()->id)}}">
                            {{csrf_field()}}
                            <div>
                              
                                    <label>Name: </label>
                                    <input id="name" type="text" name="name" required value="{{ $user-> name }}"></input>
                                     </br>    
                                    <label>Date of Birth: </label>
                                    <input type="date" id="date" name="DOB" value="{{ $user->DOB }}" required/>
                                     </br>
                                
                                    <label>phone Number : </label>
                                    <input type="text" id="phoneNumber" name="phoneNumber" value="{{ $user->phoneNumber }}" required/>
                                     </br>
                                    <label>Email : </label>
                                    <input type="email" id="email" name="email" value="{{ $user->email }}" required/>
                                    </br>
                                    <label>Password: </label>
                                    <input type="Password" id="Password" name="password" value="{{ $user->password }}" required/>
                                    </br>
                                    <button class="btn btn-dark">Save</button>
         
                                     </form>
                                 </div>
                                 </div>
                                   
     
    </div>
</section>

