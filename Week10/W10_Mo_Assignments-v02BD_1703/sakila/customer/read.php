<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

    require_once '../config/database.php';
    require_once '../customer.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $customer = new Customer($db);
    
    $stmt = $customer->read();
    $num = $stmt->rowCount();
    
    if($num>0){

    $customer_arr=array();
    $customer_arr["records"]=array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
 
        $customer_item=array(
        "customer_id" => $customer_id,
        "store_id" => $store_id,
        "first_name" => $first_name,
        "last_name" => $last_name,  
        "email" => $email, 
        "address_id" => $address_id, 
        "active" => $active, 
        "create_date" => $create_date, 
        "last_update" => $last_update 

        );
        array_push($customer_arr["records"], $customer_item);
    }
    echo json_encode($customer_arr);
}
else{
    echo json_encode(
        array("message" => "No customer found.")
    );
}
?>