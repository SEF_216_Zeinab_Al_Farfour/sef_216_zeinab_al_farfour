<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('donations', function (Blueprint $table) {
            
            $table->increments('id');
              $table->integer('user_id')-> unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('category_id')-> unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('charity_id')-> unsigned();
            $table->foreign('charity_id')->references('id')->on('charities')->onDelete('cascade');
            $table->string('description');
            $table->string('imageURL');
            $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('charity_id'); 
        $table->dropColumn('user_id'); 
        $table->dropColumn('category_id');
        Schema::dropIfExists('donations');
    }
}
