<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\recycle;
use \Illuminate\Http\Auth;
class RecycleController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $recycles = recycle::all();
        
        return view('recycle');
    }

    public function create(Request $request)
        {
            $request->validate(['address'=>'required','date'=>'required','description'=>'required','time'=>'required']);
            $recycle = new Recycle();
            $recycle->user_id=auth()->user()->id;
            $recycle->address=$request->address;
            $recycle->date=$request->date;
            $recycle->description=$request->description;
            $recycle->time=$request->time;
            $recycle->save();
           return redirect()->back();
        }


/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
      

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(post $post)
    {
        //
    }
}

