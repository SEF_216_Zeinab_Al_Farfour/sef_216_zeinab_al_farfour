@extends('layouts.header')

@section('content')


<section id="admin">
<div class="form">
<div class="container">
    <div class="row"> 
        <div class="col-md-12">
       
        <div class="table-responsive">                
              <table id="mytable" class="table table-bordred table-striped">
                   
                   <thead>
                   
                   <th><input type="checkbox" id="checkall" /></th>
                    <th>Name</th>
                     <th>Email</th>
                     <th>Password</th>
                      <th>Phone Number</th>
                       <th>DOB</th>
                   </thead>

    <tr>
    @foreach($users as $user)
    <td><input type="checkbox" class="checkthis" /></td> 
    <td>{{$user-> name}}</td>
    <td>{{$user-> email}}</td>
    <td>{{$user -> password}}</td>
    <td>{{$user -> phoneNumber}}</td>
    <td>{{$user -> DOB}}</td>   
    
    </tr>
      @endforeach
    
 
        
</table>

<div class="clearfix"></div>
</div>
</div>
</div>
</div>
</section>

@endsection