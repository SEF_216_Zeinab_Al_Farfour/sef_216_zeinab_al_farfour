@extends('layouts.app')
@section('content')
	<section id="show">
        <div class="intro"></div>
            <div class="leftrow">
                <h1>Computers</h1>
                    @foreach($data as $d)
                    <div class="blog">
                        <img src="../storage/app/{{$d -> imageURL}}"/>
                    	<h1>{{$d -> name}}</h1>
                        <p> {{$d -> brand}} </p>
                        <p> {{$d -> modelNumber}}</p>
                        <p> {{$d -> memory}}</p>
                        <p> {{$d -> cpuSpace}}</p>
                        <a href="{{ route('post.delete', ['id' => $d->id]) }}">Delete</a>
                       <div class="h-separator"></div>
                  </div>         
             </div>
                    @endforeach     
    </section>
@endsection