@extends('layouts.header')
@section('content')
<section id="intro">
  
    <div class="intro-container">

      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

        <ol class="carousel-indicators"></ol>

        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active">
            <div class="carousel-background"><img src=" ../public/img/recycling.jpeg" width="100%"></div>
            <div class="carousel-container">
              <div class="carousel-content">
                
                <h4>If you want grown-ups to recycle , just tell their kids the importance of recycling.</h4>
                <p>Bill Nye</p>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src=" ../public/img/trading.jpg" width="100%"></div>
            <div class="carousel-container">
              <div class="carousel-content">
               
                <h4>You have not lived today until you have done something for someone who can never repay you.</h4>
                 <p>John Bunyan</p>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="carousel-background"><img src=" ../public/img/donation.jpg" width="100%"></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h4> “It's not how much we give but how much love we put into giving.”</h4>
                <p>  Mother Teresa</p>
              </div>
            </div>
          </div>

        </div>

        <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>

        <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>
    </div>
  </section><!-- #intro -->

  <section id="aboutus">
                <div>
                    <a href="#" >
                        <img src="../public/img/logo.png" class="logo" />
                    </a>

                    <article class="artical">
                    <h3>About website </h3>
                    <p>iTemCare is a website  that helps you make the best  of items you don't need anymore in three different ways.
                        The user will be able to perform different actions in one website instead of using different websites and applications</p>
                    </article> 
                </div>
                </section>
       <section class="downrow">
      <div class="container__about__main">
	    <div class="grid960">
		    <div class="row">
		          <div class="col-md-4 col-sm-6">
		            <div class="square-service-block">
		               <a href="#">
		                 <div class="ssb-icon"><i class="fa fa-book" aria-hidden="true"></i></div>
		                 <h2 class="ssb-title">Trade</h2>  
		               </a>
		            </div>
		          </div>
		          
		          <div class="col-md-4 col-sm-6">
		            <div class="square-service-block">
		               <a href="#">
		                 <div class="ssb-icon"> <i class="fa fa-heart" aria-hidden="true"></i> </div>
		                 <h2 class="ssb-title">Donate</h2>  
		               </a>
		            </div>
		          </div>
		            <div class="col-md-4 col-sm-6">
		            <div class="square-service-block">
		               <a href="#">
		                 <div class="ssb-icon"> <i class="fa fa-recycle" aria-hidden="true"></i> </div>
		                 <h2 class="ssb-title">recycle</h2>  
		               </a>
		            </div>
		          </div>
		    </div>
	    </div>
		</div>
    </section>

		<section id="cause">
			<div class="title"><h1>OUR CAUSES</h1></div>
			<div class="h_seperator" ></div>
			<div class="box">
				<img src="../public/img/world.jpeg"/>
				<h4>TO MAKE A BETTER PLACE </h4>
				<p>Magna nulla et aliquip enim minim ea ad mollit. Id non cillum ad tempor deserunt ut ipsum.</p>
				
			</div>
			<div class="box">
				<img src="../public/img/remove.png"/>
				<h4>REMOVE UNWANTED ITEMS </h4>
				<p>Magna nulla et aliquip enim minim ea ad mollit. Id non cillum ad tempor deserunt ut ipsum.</p>
			</div>
			<div class="box">
					<img src="../public/img/help.jpeg"/>
				<h4>HElP THE ENVIROMENT </h4>
				<p>Magna nulla et aliquip enim minim ea ad mollit. Id non cillum ad tempor deserunt ut ipsum.</p>
			</div>

		</section>
		<section id="footer">
                <div class="grid960a">

                    <div class="container__footer__column-left">
                    
 
                        <p>Lorem ipsum dolor sit amet, consecteturadipisicing elit. Excepturi quasi delectus iusto incidunt.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                       
                    </div>
                    <div class="container__footer__column-middle">
                        <h3>OUR STUDIO</h3>
                        <table class="container__footer__column-middle__location">
                            <tr>

                                <td>
                                    <p>Lorem ipsum dolor sit amet, consecteturadipisicing elit. Excepturi quasi delectus</p>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <p>(+62) 21-2224 3333</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                
                    <div class="container__footer__column-right">
                        <h3>STAY IN TOUCH</h3>
                        <input type="text" value="Subscribe our newsletter">
                        
                        <div class="container__footer__column-right__media ">
                           
                        </div>
                          
                    </div>
                
                </div>
            </section><!--/END container__footer -->
@endsection